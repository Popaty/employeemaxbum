package com.skoode.services;

import com.skoode.model.Employee;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Maximiliano on 10/7/2015 AD.
 */
public class EmployeeServicesTest {

    private EmployeeServices employeeServices;

    @Before
    public void setUp() throws Exception {
        employeeServices = new EmployeeServices();

    }

    @After
    public void tearDown() throws Exception {
        employeeServices = null;

    }

    @org.junit.Test
    public void testGetAllEmployee() throws Exception {
        List<Employee> employeeList = employeeServices.getAllEmployee();
        Assert.assertNotNull(employeeList);
        for (Employee employee : employeeList){
//            System.out.println(employee);
            System.out.println(employeeServices.calAge(employee.getDob()));

        }
    }

    @Test
    public void testGetAllEmployeeAndAge() throws Exception {
        List<Employee> employeeList = employeeServices.getAllEmployeeAndAge();
        Assert.assertNotNull(employeeList);
        for (Employee employee : employeeList){
            System.out.println(employee);

        }
    }

    @Test
    public void testGetEmployeeBySearch() throws Exception {
        List<Employee> employeeList = employeeServices.getEmployeeBySearch("po");
        Assert.assertNotNull(employeeList);
        for (Employee employee : employeeList){
            System.out.println(employee);

        }
    }

    @Test
    public void testInsertEmployee() throws Exception {

    }

    @Test
    public void testGetEmployeeById() throws Exception {
        System.out.println(employeeServices.getEmployeeById(1));
    }
}
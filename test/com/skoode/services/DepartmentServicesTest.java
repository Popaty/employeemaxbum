package com.skoode.services;

import com.skoode.model.Department;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Maximiliano on 10/8/2015 AD.
 */
public class DepartmentServicesTest {

    private DepartmentServices departmentServices;

    @Before
    public void setUp() throws Exception {
        departmentServices = new DepartmentServices();

    }

    @After
    public void tearDown() throws Exception {
        departmentServices = null;

    }

    @Test
    public void testGetAllDepartment() throws Exception {
        List<Department> departmentList = departmentServices.getAllDepartment();
        for (Department department : departmentList){
            System.out.println(department);
        }
    }

    @Test
    public void testGetDepartmentById() throws Exception {
        System.out.println(departmentServices.getDepartmentById(1));
    }
}
package com.skoode.model;

/**
 * Created by Maximiliano on 10/8/2015 AD.
 */
public class Telephone {
    private Employee employee;
    private String phoneNumber;

    @Override
    public String toString() {
        return "Telephone{" +
                "employee=" + employee +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}

package com.skoode.mappers;

import com.skoode.model.Department;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by Maximiliano on 10/8/2015 AD.
 */
public interface DepartmentMapper {

    @Select("select * from department")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "name", column = "department_name")
    })
    public List<Department> getAllDepartment();

    @Select("select * from department where id = #{id}")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "name", column = "department_name")
    })
    public Department getDepartmentById(int id);
}

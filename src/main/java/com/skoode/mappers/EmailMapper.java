package com.skoode.mappers;

import org.apache.ibatis.annotations.Insert;

/**
 * Created by Maximiliano on 10/9/2015 AD.
 */
public interface EmailMapper {
    @Insert("insert into email (employee_id, email) " +
            "VALUES (#{param1},#{param2})")
//    @Options(useGeneratedKeys = true, keyProperty = "id")
    public void insertEmail(int empId, String email);
}

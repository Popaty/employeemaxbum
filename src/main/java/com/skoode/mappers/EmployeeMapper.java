package com.skoode.mappers;

import com.skoode.model.Employee;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by Maximiliano on 10/7/2015 AD.
 */
public interface EmployeeMapper {

    @Select("select * from employee")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "nickName", column = "nick_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "dob", column = "dob"),
            @Result(property = "salary", column = "salary"),
            @Result(property = "status", column = "status"),
            @Result(property = "department", column = "department_id",
                    one = @One(select = "com.skoode.mappers.DepartmentMapper.getDepartmentById"))
    })
    public List<Employee> getAllEmployee();

    @Select("select * from employee " +
            "where first_name " +
            "like CONCAT('%', #{param1}, '%') OR nick_name " +
            "like CONCAT('%', #{param1}, '%')")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "nickName", column = "nick_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "dob", column = "dob"),
            @Result(property = "salary", column = "salary"),
            @Result(property = "status", column = "status"),
            @Result(property = "department", column = "department_id",
                    one = @One(select = "com.skoode.mappers.DepartmentMapper.getDepartmentById"))
    })
    public List<Employee> getEmployeeBySearch(String searchtext);

    @Insert("insert into employee (first_name,last_name,nick_name,gender,dob,department_id,salary,status)" +
            "values (#{firstName},#{lastName},#{nickName},#{gender},#{dob},#{department.id},#{salary},#{status})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public int insertEmployee(Employee employee);

    @Select("select * from employee where id = #{id}")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "nickName", column = "nick_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "dob", column = "dob"),
            @Result(property = "salary", column = "salary"),
            @Result(property = "status", column = "status"),
            @Result(property = "department", column = "department_id",
                    one = @One(select = "com.skoode.mappers.DepartmentMapper.getDepartmentById"))
    })
    public Employee getEmployeeById(int id);

//    @Insert("INSERT INTO EMPLOYEE (FIRST_NAME,LAST_NAME,DOB,EMAIL,DEPARTMENT_ID) " +
//            "VALUES (#{firstName},#{lastName},#{dob},#{email},#{department.id})")
//    @Options(useGeneratedKeys = true, keyProperty = "id")
//    public void insertEmployee(Employee employee);
}

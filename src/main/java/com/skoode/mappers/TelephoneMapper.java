package com.skoode.mappers;

import com.skoode.model.Employee;
import com.skoode.model.Telephone;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;

/**
 * Created by Maximiliano on 10/8/2015 AD.
 */
public interface TelephoneMapper {
    @Insert("insert into telephone (employee_id, phone_number) " +
            "VALUES (#{param1},#{param2})")
//    @Options(useGeneratedKeys = true, keyProperty = "id")
    public void insertTelephone(int empId, String phoneNo);
}

package com.skoode.services;

import com.skoode.mappers.TelephoneMapper;
import com.skoode.model.Telephone;
import org.apache.ibatis.session.SqlSession;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 * Created by Maximiliano on 10/8/2015 AD.
 */
@ManagedBean(name = "telephone")
@ApplicationScoped
public class TelephoneServices implements TelephoneMapper{
    @Override
    public void insertTelephone(int empId, String phoneNo) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            TelephoneMapper telephoneMapper = sqlSession.getMapper(TelephoneMapper.class);
            telephoneMapper.insertTelephone(empId,phoneNo);
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
    }
}

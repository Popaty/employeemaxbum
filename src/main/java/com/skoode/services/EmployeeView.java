package com.skoode.services;

import com.skoode.model.Department;
import com.skoode.model.Employee;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Maximiliano on 10/8/2015 AD.
 */
@ManagedBean(name="employeeView")
@ViewScoped
public class EmployeeView {
    private List<Employee> employeeList;
    private List<Employee> filteredEmployee;
    private EmployeeServices employeeServices = new EmployeeServices();
    private Employee employeeEntity;
    private Boolean statusEmployee = false;
    private String statusResult;
    private Employee selectedEmployee;

    @ManagedProperty("#{telephoneView}")
    private TelephoneView telephoneView;

    @ManagedProperty("#{emailView}")
    private EmailView emailView;

    @PostConstruct
    public void init() {
        employeeList = employeeServices.getAllEmployeeAndAge();
    }

    public Employee getSelectedEmployee() {
        return selectedEmployee;
    }

    public void setSelectedEmployee(Employee selectedEmployee) {
        this.selectedEmployee = selectedEmployee;
    }

    public String getStatusResult() {
        return statusResult;
    }

    public void setStatusResult(String statusResult) {
        this.statusResult = statusResult;
    }

    public Boolean getStatusEmployee() {
        return statusEmployee;
    }

    public void setStatusEmployee(Boolean statusEmployee) {
        this.statusEmployee = statusEmployee;
    }

    public EmailView getEmailView() {
        return emailView;
    }

    public void setEmailView(EmailView emailView) {
        this.emailView = emailView;
    }

    public TelephoneView getTelephoneView() {
        return telephoneView;
    }

    public void setTelephoneView(TelephoneView telephoneView) {
        this.telephoneView = telephoneView;
    }

    public String newEmployee(){
        System.out.println(employeeEntity);
//        System.out.println(telephoneView.getTelNoList());
//        System.out.println(emailView.getEmailList());
        FacesContext facesContext = FacesContext.getCurrentInstance();
        DepartmentServices departmentServicesBean = (DepartmentServices) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext,"department");
        TelephoneServices telephoneServicesBean = (TelephoneServices) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext,"telephone");
        EmailServices emailServicesBean = (EmailServices) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext,"email");

        int departmentId = departmentServicesBean.getSelectedDepartment();
        Department department = departmentServicesBean.getDepartmentById(departmentId);
        
        employeeEntity.setDepartment(department);
        if(!statusEmployee){
            employeeEntity.setStatus("not-hired");
        }else {
            employeeEntity.setStatus("hired");
        }
//        employeeEntity.setStatus(statusResult);
//        System.out.println(departmentId);
        employeeServices.insertEmployee(employeeEntity);
//        System.out.println(employeeEntity.getId());
        List<String> telephoneNo = telephoneView.getTelNoList();
        if (!telephoneNo.isEmpty()){
            for (String telephone : telephoneNo){
                telephoneServicesBean.insertTelephone(employeeEntity.getId(),telephone);
            }
        }

        List<String> emailList = emailView.getEmailList();
        if (!emailList.isEmpty()){
            for (String email : emailList){
                emailServicesBean.insertEmail(employeeEntity.getId(), email);
            }
        }
        return "/index.xhtml?faces-redirect=true";
    }

    public void startForm(){
        employeeEntity = new Employee();
    }

    public void editForm(){
        employeeEntity = new Employee();
        String stringId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        int editId = Integer.parseInt(stringId);
        System.out.println("Before "+employeeEntity);
        employeeEntity = employeeServices.getEmployeeById(editId);
        System.out.println("After "+employeeEntity);

        FacesContext facesContext = FacesContext.getCurrentInstance();
        DepartmentServices departmentServicesBean = (DepartmentServices) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext,"department");
        departmentServicesBean.setSelectedDepartment(employeeEntity.getDepartment().getId());
//        departmentServicesBean.getDepartmentById(employeeEntity.getDepartment().getId()).getId())
//        System.out.println(s);
//        employeeEntity.setFirstName(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("firstname"));
    }

    public void setEmployeeStatus(){
        statusResult = statusEmployee ? "hired" : "not-hired";
    }

    public Employee getEmployeeEntity() {
        return employeeEntity;
    }

    public void setEmployeeEntity(Employee employeeEntity) {
        this.employeeEntity = employeeEntity;
    }

//    public EmployeeView(){
//        employeeList = employeeServices.getAllEmployeeAndAge();
//    }

    public List<Employee> getFilteredEmployee() {
        return filteredEmployee;
    }

    public void setFilteredEmployee(List<Employee> filteredEmployee) {
        this.filteredEmployee = filteredEmployee;
    }



    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

//    public void getparams() throws IOException {
//        FacesContext fc = FacesContext.getCurrentInstance();
//        Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
//        System.out.println(params);
//
//        employeeEntity = new Employee();
//        employeeEntity.setFirstName(params.get("firstname"));
//
//
//        System.out.println(employeeEntity);
//        FacesContext.getCurrentInstance().getExternalContext().redirect("/EditemployeePage.xhtml?faces-redirect=true");
////        return "/EditemployeePage.xhtml?faces-redirect=true";
//    }
//    public void onRowSelect(SelectEvent event) throws IOException {
////        employeeEntity = selectedEmployee;
////        System.out.println(employeeEntity);
////        FacesContext.getCurrentInstance().getExternalContext().redirect("/EditemployeePage.xhtml?faces-redirect=true");
////        return params.get("country");
////        System.out.println(selectedEmployee);
////        return "/employeePage.xhtml?faces-redirect=true";
//        FacesContext fc = FacesContext.getCurrentInstance();
//        Employee employee = fc.getApplication().evaluateExpressionGet(fc,"",Employee.class);
////        Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
//        System.out.println(employee);
////        System.out.println(params);
//    }
//
//    public void onRowUnselect(UnselectEvent event) {
//        System.out.println(selectedEmployee);
//    }
}

package com.skoode.services;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maximiliano on 10/8/2015 AD.
 */
@ManagedBean(name="telephoneView")
@ViewScoped
public class TelephoneView {

    private List<String> telNoList = new ArrayList<>();
    private String telNo;

    public void addNoToList(){
        telNoList.add(telNo);
    }

    public String getTelNo() {
        return telNo;
    }

    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }

    public List<String> getTelNoList() {
        return telNoList;
    }

    public void setTelNoList(List<String> telNoList) {
        this.telNoList = telNoList;
    }
}

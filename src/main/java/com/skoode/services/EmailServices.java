package com.skoode.services;

import com.skoode.mappers.EmailMapper;
import com.skoode.model.Email;
import org.apache.ibatis.session.SqlSession;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 * Created by Maximiliano on 10/9/2015 AD.
 */
@ManagedBean(name = "email")
@ApplicationScoped
public class EmailServices implements EmailMapper {
    @Override
    public void insertEmail(int empId, String email) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmailMapper emailMapper = sqlSession.getMapper(EmailMapper.class);
            emailMapper.insertEmail(empId,email);
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
    }
}

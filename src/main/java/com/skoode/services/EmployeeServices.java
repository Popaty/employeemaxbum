package com.skoode.services;

import com.skoode.mappers.EmployeeMapper;
import com.skoode.model.Employee;
import org.apache.ibatis.session.SqlSession;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Maximiliano on 10/7/2015 AD.
 */
@ManagedBean (name = "employee")
@ApplicationScoped
public class EmployeeServices implements EmployeeMapper {

    @Override
    public List<Employee> getAllEmployee() {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            return employeeMapper.getAllEmployee();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<Employee> getEmployeeBySearch(String searchtext) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            List<Employee> employeeList = new ArrayList<>();
            for (Employee employee : employeeMapper.getEmployeeBySearch(searchtext)){
                employee.setAge(calAge(employee.getDob()));
                employeeList.add(employee);
            }
            return employeeList;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int insertEmployee(Employee employee) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            int effectrow = employeeMapper.insertEmployee(employee);
            sqlSession.commit();
            return effectrow;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public Employee getEmployeeById(int id) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            return employeeMapper.getEmployeeById(id);
        } finally {
            sqlSession.close();
        }
    }

    public List<Employee> getAllEmployeeAndAge() {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            List<Employee> employeeList = new ArrayList<>();
            for (Employee employee : employeeMapper.getAllEmployee()){
                employee.setAge(calAge(employee.getDob()));
                employeeList.add(employee);
            }
            return employeeList;
        } finally {
            sqlSession.close();
        }
    }

    public int calAge(Date dateOfBirth) {

        Calendar today = Calendar.getInstance();
        Calendar birthDate = Calendar.getInstance();

        int age = 0;

        birthDate.setTime(dateOfBirth);
//        if (birthDate.after(today)) {
//            throw new IllegalArgumentException("Can't be born in the future");
//        }

        age = today.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);

        // If birth date is greater than todays date (after 2 days adjustment of leap year) then decrement age one year
        if ( (birthDate.get(Calendar.DAY_OF_YEAR) - today.get(Calendar.DAY_OF_YEAR) > 3) ||
                (birthDate.get(Calendar.MONTH) > today.get(Calendar.MONTH ))){
            age--;

            // If birth date and todays date are of same month and birth day of month is greater than todays day of month then decrement age
        }else if ((birthDate.get(Calendar.MONTH) == today.get(Calendar.MONTH )) &&
                (birthDate.get(Calendar.DAY_OF_MONTH) > today.get(Calendar.DAY_OF_MONTH ))){
            age--;
        }

        return age;
    }






}

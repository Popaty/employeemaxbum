package com.skoode.services;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maximiliano on 10/9/2015 AD.
 */
@ManagedBean(name="emailView")
@ViewScoped
public class EmailView {

    private List<String> emailList = new ArrayList<>();
    private String email;

    public void addEmailToList(){
        emailList.add(email);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<String> emailList) {
        this.emailList = emailList;
    }
}

package com.skoode.services;

import org.primefaces.model.UploadedFile;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * Created by warisara on 10/13/2015 AD.
 */
@ManagedBean (name = "testImage")
@SessionScoped
public class testImageServices {

    private UploadedFile uploadedFile;

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }
    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }


    public void upload(){

            System.out.println(uploadedFile);
            if(uploadedFile != null) {
                byte[] imagebyte = uploadedFile.getContents();
//                String name = uploadedFile.getFileName();
                UUID name = UUID.randomUUID();

                ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
                String nameImage = servletContext.getRealPath("/resources/images/") +name+".jpg";
                System.out.println(nameImage);

                try {
                    BufferedImage img = ImageIO.read(new ByteArrayInputStream(imagebyte));
                    File testFile = new File(nameImage);
                    ImageIO.write(img, "jpg", testFile);


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }else {
                System.out.println("This uploaded file is Null");
            }

    }

}

package com.skoode.services;

import com.skoode.mappers.DepartmentMapper;
import com.skoode.model.Department;
import org.apache.ibatis.session.SqlSession;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.List;

/**
 * Created by Maximiliano on 10/8/2015 AD.
 */
@ManagedBean(name = "department")
@ApplicationScoped
public class DepartmentServices implements DepartmentMapper{

    private int selectedDepartment;

    public int getSelectedDepartment() {
        return selectedDepartment;
    }

    public void setSelectedDepartment(int selectedDepartment) {
        this.selectedDepartment = selectedDepartment;
    }

    @Override
    public List<Department> getAllDepartment() {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            DepartmentMapper departmentMapper = sqlSession.getMapper(DepartmentMapper.class);
            return departmentMapper.getAllDepartment();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public Department getDepartmentById(int id) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            DepartmentMapper departmentMapper = sqlSession.getMapper(DepartmentMapper.class);
            return departmentMapper.getDepartmentById(id);
        } finally {
            sqlSession.close();
        }
    }
}
